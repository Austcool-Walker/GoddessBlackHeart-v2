#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "libgo/hstd.h"
// This includes everything that main.go includes
#include "libgo/libgo.h"

U0 nothing(I32 sig) {
	printf("Shutting down.\n");
	exit(1);
}

U0 shutdown(I32 sig) {
	printf("Shutting down bot.\n");
	stop();
	exit(0);
}

I32 main() {
	signal(SIGINT, nothing); 
	loadConfig();
	start();
	signal(SIGINT, shutdown); 
	printf("Bot is up CTRL-C (SIGINT) to stop.\n");
	hhalt();
	return 1;
}
