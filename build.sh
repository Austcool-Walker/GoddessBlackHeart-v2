#!/bin/sh

if [ ! -f config.txt ]; then
echo "Copying example config to config.txt..."
cp config.example.txt config.txt
echo "Copied! Please remember to change the token in config.txt before running"
fi

export CC="clang"
export CCX="clang++"

case "$1" in
    clean)
        rm libgo/libgo.h libgo/libgo.so libgo/libgo.dll ChadBot; exit;;
esac

echo "Building libgo..."
cd libgo
# If you're having issues make it a shared library and just link to it
go build -buildmode c-archive -o libgo.so

echo "Building and linking to the rest of the project..."
cd ..
$CC main.c -pthread -Llibgo -lgo -o ChadBot
