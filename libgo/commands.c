#include <stdio.h>
#include <string.h>
#include "util.h"
#include "commands.h"
#include "config.h"
#include "_cgo_export.h"

U0 commands(U8 *discordSession, U8 *discordMessage, C8 *contents, C8 *botID, C8 *userID) {
	if (contents[0] != prefix || !strcmp(botID, userID))
		return;
	if (!strcmp(&contents[1], "ping")) ping(discordSession, discordMessage);
	else if (!strcmp(&contents[1], "pong")) ping(discordSession, discordMessage);
}
