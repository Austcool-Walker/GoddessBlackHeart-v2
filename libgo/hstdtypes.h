#ifdef __cplusplus
extern "C" {
#endif

// HolyC Data Types
// C8 provides a exact char for portable code
#ifdef hstdtypesDirect
typedef void U0;
typedef void I0;
typedef char C8;
typedef signed char I8;
typedef unsigned char U8;
typedef short I16;
typedef unsigned short U16;
typedef int I32;
typedef unsigned int U32;
typedef long long I64;
typedef unsigned long long U64;
typedef float F32;
typedef double F64;
typedef long double F128;
#else
#include <stdint.h>
typedef void I0;
typedef void U0;
typedef char C8;
typedef int8_t I8;
typedef uint8_t U8;
typedef int16_t I16;
typedef uint16_t U16;
typedef int32_t I32;
typedef uint32_t U32;
typedef int64_t I64;
typedef uint64_t U64;
// idk the fixed width type for float
typedef float F32;
typedef double F64;
typedef long double F128;
#endif

#ifdef __cplusplus
}
#endif
