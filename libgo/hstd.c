#include "hstd.h"
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

U0 hhalt() {
#ifdef _WIN32
	Sleep(INFINITE);
#else
	while (1) {
		pause();
	}
#endif
}

