#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"
#include "_cgo_export.h"

U0 errorf(const C8 *text) {
	fprintf(stderr, "%s", text);
	fflush(stderr); // stderr and stdout is flushed on exit iirc but just incase
	// We do it anyway
	exit(1);
}

U0 printError(const C8 *text) {
	fprintf(stderr, "%s", text);
	fflush(stderr);
}

U0 printChar(const C8 *text) {
	printf("%s", text);
	fflush(stdout);
}

U8 startsWith(const C8 *a, const C8 *b)
{
   if(strncmp(a, b, strlen(b)) == 0) return 1;
   return 0;
}

// Convert C char pointer to Go string without making a copy
GoString GGoStringN(C8 *pointer, U64 leng) {
	GoString go_str;
	go_str.p = pointer;
	go_str.n = leng;
	return go_str;
}
GoString GGoString(C8 *pointer) {
	return GGoStringN(pointer, strlen(pointer));
}

// Convert Go String to C char pointer without making a copy
const C8 *GCString(GoString go_str) {
	return go_str.p;
}

